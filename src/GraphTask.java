import java.util.*;
/**
 * Ülesandeks on "Koostada meetod, mis arvutab etteantud sidusa lihtgraafi G=(V,E) iga tipu
 * v jaoks välja selle ekstesntrilisuse:
 * 	e(v) = max {d(u,v)| u kuulub hulka V}, kus d(u,v) on tipu u kaugus tipust v
 * 	(lühima tee pikkus tippust u tippu v)
*/

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
	final static int INF = 99999, V = 4;
	
   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      
      
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph("G");
      int[][] adjMatrix=g.createRandomSimpleGraph(6, 6);
      System.out.println(g.toString());
      System.out.println(adjMatrix.length + "|\t" + "Ekstsentrilisus");
      int[][] distmatrix=g.distMatrix(adjMatrix);

      System.out.println();
      for (int k = 0; k < distmatrix.length; k++) {
    	  int max = getMax(distmatrix[k]);
    	  System.out.print(k + " |\t" + max + "|\t");
    	  for (int j = 0; j < distmatrix[k].length; j++) {
    		  System.out.print(distmatrix[k][j] + " ");
    	  }
    	  System.out.print("\n");
      }
}
      // TODO!!! Your experiments here

private int getMax(int[] distmatrix) {
	// TODO Auto-generated method stub
	int maxValue = 0;
		for(int k = 0;  k < distmatrix.length; k++){
			if(distmatrix[k] > maxValue){
				maxValue = distmatrix[k];
			}
		}
	return maxValue;
}
   }

   /** 
    * Vertex reprents the tip in the graph.
    * @author slava
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         setNext(v);
         setFirst(e);
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

	public Arc getFirst() {
		return first;
	}

	public void setFirst(Arc first) {
		this.first = first;
	}

	public Vertex getNext() {
		return next;
	}

	public void setNext(Vertex next) {
		this.next = next;
	}

	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         setTarget(v);
         setNext(a);
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

	public Vertex getTarget() {
		return target;
	}

	public void setTarget(Vertex target) {
		this.target = target;
	}

	public Arc getNext() {
		return next;
	}

	public void setNext(Arc next) {
		this.next = next;
	}

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.getFirst();
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.getTarget().toString());
               sb.append (")");
               a = a.getNext();
            }
            sb.append (nl);
            v = v.getNext();
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.setNext(first);
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.setNext(from.getFirst());
         from.setFirst(res);
         res.setTarget(to);
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.setInfo(info++);
            v = v.getNext();
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.getInfo();
            Arc a = v.getFirst();
            while (a != null) {
               int j = a.getTarget().getInfo();
               res [i][j]++;
               a = a.getNext();
            }
            v = v.getNext();
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
     * @return 
       */
      public int[][] createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
        	 throw new IllegalArgumentException
        	 ("Impossible number of vertices: " + n);
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.getNext();
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         return connected;
      }

      /**
       * Converts adjecency matrix to the matrix of distances.
       * @param int[][] adjmatrix
       * @return matrix of distance, where edge has length 1
       * @implemented Floyd-Warshall algorithm
       * @author slava
       */
      
      public int[][] distMatrix(int[][] adjmatrix) {
    	  boolean elements=false;
    	  	for (int i = 0; i < adjmatrix.length; i++) {
    	  		for (int j = 0; j < adjmatrix[i].length; j++) {
    	  			if(adjmatrix[i][j]==1){
    	  				elements=true;
    	  				}
    	  			}
    	  		}
    	  	if(elements==false){
    	  		throw new RuntimeException("Graafis puuduvad seosed");
    	  		}
    	  	int nvert = adjmatrix.length;
    	  	int[][] result = new int[nvert][nvert];

    	  	if (nvert < 1)
    	  		return result;
    	  int INFINITY = 2 * nvert + 1; // Integer.MAX_VALUE / 2; // NB!!!
    	  for (int i = 0; i < nvert; i++) {
    		  for (int j = 0; j < nvert; j++) {
    			  if (adjmatrix[i][j] == 0) {
    				  result[i][j] = INFINITY;
    			  } else {
    				  result[i][j] = 1;
    			  }
    		  }
    	  }
    	  for (int i = 0; i < nvert; i++) {
    		  result[i][i] = 0;
    	  	}

    	  int n = result.length; // number of vertices
    	  	for (int k = 0; k < n; k++) {
    	  		for (int i = 0; i < n; i++) {
    	  			for (int j = 0; j < n; j++) {
    	  				int newlength = result[i][k] + result[k][j];
    	  				if (result[i][j] > newlength) {
    	  					result[i][j] = newlength; // new path is shorter
    	  				}
    	  			}
    	  		}
    	  	}

    	  return result;
  		}
}